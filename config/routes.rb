MarketingSite::Engine.routes.draw do
	get '/' => 'pages#index', slug: 'home'
  get '/:slug' => 'pages#show'
  resources :pages
end
