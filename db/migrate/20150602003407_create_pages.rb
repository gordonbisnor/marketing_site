class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :content
      t.string :layout
      t.boolean :published
      t.string :slug

      t.timestamps null: false
    end
  end
end
