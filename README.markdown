Marketing Site
==============

Install
-------

Gemfile:

```
gem 'marketing_site', git: 'https://bitbucket.org/gordonbisnor/marketing_site' 
gem 'tinymce-rails'
```

Terminal:
```
bundle exec rake db:migrate
```

config/routes.rb:

```
Rails.application.routes.draw do
  constraints(:subdomain => /www/) do  
    mount MarketingSite::Engine, at: "/"
  end
end
```

config/tinymce.yml
```
verify_html: false
valid_elements: "*[*]"
extended_valid_elements: "*[*]"
media_strict: false
width: 800px
height: 400px
fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 24px 32px 36px 48px 60px 72px"
plugins:
  - fullpage
  - paste
  - image
  - link
  - fullscreen
  - anchor
  - code
  - colorpicker
  - hr
  - searchreplace
  - table
  - textcolor
toolbar: "forecolor backcolor fontsizeselect alignleft aligncenter alignright alignjustify fullpage"
paste_as_text: true
paste_word_valid_elements: true
theme_advanced_buttons3_add: "fullpage"

```

config/initializers/rails_admin.rb:

```

  config.model "Page" do
    configure :content do
      html_attributes rows: 20, cols: 50      
    end
    list do
      field :title
      field :slug
      field :layout
      field :published
    end
    edit do
      field :layout
      field :title
      field :slug
      #field :content, :text
      field :content do
        partial 'pages/content_field'
      end
      field :published
    end
  end

  config.model "Asset" do
    list do
      field :url do
        formatted_value{ bindings[:object].url } 
      end
    end
  end

```

app/views/rails_admin//main/pages/_content_field.html.haml:

```
= tinymce_assets
= form.text_area :content, class: 'tinymce' 
= tinymce 
```

app/controllers/application_controller.rb:

```

  def layout
  	use_empty_layout? ? 'empty' : "application"
  end
  private :layout

  def use_empty_layout?
  	[
  	  MarketingSite::PagesController
  	].include? self.class
  end

```