module MarketingSite

	class PagesController < ApplicationController
		
		def index
			@page = Page.find_by(slug: 'home')	
			render template: 'marketing_site/pages/show', layout: false
		end

		def show
		  @page = Page.published.find_by(slug: params[:slug])
		  raise ActiveRecord::RecordNotFound if @page.blank?

		  if @page.layout == 'none' 
		  	render layout: false
		  elsif @page.layout.present?
		  	render layout: @page.layout
		  end
		  
		end

	end

end
