FactoryGirl.define do
  factory :page do
    title "MyString"
		content "MyString"
		layout "MyString"
		published false
		slug "MyString"
  	trait :published do
  	  published true
  	end
    trait :unpublished do
      published false
    end
  end
end
