require 'rails_helper'

RSpec.describe "marketing_site/pages/show.html.erb", type: :view do
	
	let(:page) { FactoryGirl.build_stubbed(:page) }	
	
	before do
	  assign(:page, page)
	end

 	it "renders" do
 		render
 		expect(rendered).to match(page.content)
 	end

end
