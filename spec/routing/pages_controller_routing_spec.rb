require "rails_helper"


RSpec.describe MarketingSite::PagesController, :type => :routing do
  routes { MarketingSite::Engine.routes }

  it "routes to index" do
    expect(:get => '/').
      to route_to(:controller => "marketing_site/pages", :action => "index", slug: 'home')
  end

  it "routes to index" do
    expect(:get => '/conference').
      to route_to(:controller => "marketing_site/pages", :action => "show", slug: 'conference')
  end

end
