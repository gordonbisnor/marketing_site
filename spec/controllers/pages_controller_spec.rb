require 'rails_helper'

describe MarketingSite::PagesController do

  routes { MarketingSite::Engine.routes }

	describe "GET index" do
		before do
			@page = FactoryGirl.create(:page, slug: 'home')
			get :index
		end
		it { is_expected.to render_template(:show) }
		it { is_expected.to respond_with(:success) }
		specify { expect(assigns[:page]).to eql(@page) }
	end

	describe "GET show" do
		context "with layout" do
			before do
				@page = FactoryGirl.create(:page, :published, slug: 'conference', layout: 'conference')
				get :show, slug: @page.slug
			end
			it { is_expected.to render_template(:show) }
			it { is_expected.to respond_with(:success) }
			specify { expect(assigns[:page]).to eql(@page) }
			specify { expect(response).to render_template "layouts/conference" }
		end
		context "with no layout" do
			before do
				@page = FactoryGirl.create(:page, :published, slug: 'conference', layout: 'none')
				get :show, slug: @page.slug
			end
			it { is_expected.to render_template(:show) }
			it { is_expected.to respond_with(:success) }
			specify { expect(assigns[:page]).to eql(@page) }
			specify { expect(response).to render_template "pages/show" }
		end
	end

end



