require 'rails_helper'

RSpec.describe Page, type: :model do
  it "should be valid from the factory" do
  	expect(FactoryGirl.create(:page).class).to equal(Page)
  end
  
  it "finds published items" do
		@published = FactoryGirl.create(:page, :published)
		@unpublished = FactoryGirl.create(:page, :unpublished)
		expect(Page.published).to include(@published)
		expect(Page.published).to_not include(@unpublished)
  end

end
