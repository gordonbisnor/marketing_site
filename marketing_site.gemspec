$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "marketing_site/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "marketing_site"
  s.version     = MarketingSite::VERSION
  s.authors     = ["Gordon B. Isnor"]
  s.email       = ["gordonbisnor@gmail.com"]
  s.homepage    = "http://github.com/gordonbisnor/marketing_site"
  s.summary     = "Marketing site engine"
  s.description = "Marketing site engine"
  s.license     = "NO LICENSE"

  s.files = Dir["{app,config,db,lib}/**/*", "NO-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", '< 6.1'
  s.add_dependency "paperclip"
  s.add_development_dependency "sqlite3"
end
